clear all
close all
clc
move_numbers=[1 5 20];
time_vec=100:100:1000;
for index = 1:length(move_numbers)
    Move_Number=move_numbers(index);
    load(sprintf('Long_MaxAggPO_%d.mat',min(999,Move_Number)),'PO','N');
    cumPO=cumsum(PO,3);
    for print_index=1:length(time_vec)
        finalcumPO=squeeze(cumPO(:,:,time_vec(print_index)));
        N_runs=size(finalcumPO,1);
        gi=zeros(N_runs,1);
        for ind=1:N_runs
            gi(ind)=gini(ones(1,N),finalcumPO(ind,:)/time_vec(print_index));
        end
        GiniCoeff(print_index,index)=mean(gi);
    end
end

colorvex={'b-x','r-x','o-x'};
figure
for index = 1:length(move_numbers)
    plot(time_vec,GiniCoeff(:,index),'-x')
    hold on
end
xlabel('$T$','Interpreter','latex')
ylabel('Gini Coefficient','Interpreter','latex')
legend({'$k=1$','$k=5$','$k=20$'},'Interpreter','latex')
