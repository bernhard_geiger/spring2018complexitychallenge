function pool=chooseOldPool(payout,currentPool,tau)
payout(isinf(payout))=nan;
payout=mean(payout,1,'omitnan');
payout=payout-tau;
payout(currentPool)=payout(currentPool)+tau;

pools=find(payout==max(payout));
pool=pools(randi(1:length(pools)));