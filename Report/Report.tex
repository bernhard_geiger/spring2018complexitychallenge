%% Draft mode
% \newcommand{\CLASSINPUTinnersidemargin}{17mm}
% \documentclass[a4paper,draftcls,onecolumn]{IEEEtran}
% %% Submission mode
\documentclass[a4paper,technote,12pt,onecolumn]{IEEEtran}
\usepackage{setspace}
\onehalfspacing


\usepackage{etex}
\usepackage{graphicx,ifthen}
\usepackage{psfrag,amssymb,amsthm}
\usepackage[cmex10]{amsmath}

%\usepackage{url}
%\usepackage{epstopdf}
\usepackage{mathtools}
\usepackage[noadjust]{cite}


\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{xspace}
\usepackage{xcolor}
\usepackage{url}
\usepackage{framed}
\usepackage{float}
\usepackage{rotating}
\usepackage{verbatim}
\usepackage{listings}
\usepackage{lscape}
\usepackage{pstricks,pst-sigsys,pst-plot}
\usepackage{subfigure}
\usepackage{algpseudocode,algorithm,algorithmicx}


 \usepackage{pgfplots}
%  \usepackage{pgf}
 \usepackage{tikz}
 \usetikzlibrary{arrows,shapes.misc,chains,scopes,fit}
%  \pgfplotsset{compat = newest}
%  \usepackage{pgfplotstable}

 \usepackage{booktabs}
 \usepackage{colortbl}


\newtheorem{thm}{Theorem}
\newtheorem{lem}{Lemma}
\newtheorem{cor}{Corollary}
\newtheorem{prop}{Proposition}
\newtheorem{remark}{Remark}
\theoremstyle{definition}
\newtheorem{definition}{Definition}
\newtheorem{assumption}{Assumption}
\newtheorem{example}{Example}

\newcommand{\cbg}[1]{{\color{red} #1}}

% \setlength{\bottommargin}      {-1.5cm}
\setlength{\textheight}     {25cm} 
% \addtolength{\textwidth}{4.5cm}
% \addtolength{\hoffset}{-2.25cm}



%opening
\title{{\huge Randomness is Hard to Beat}\\Entry for the Spring 2018 Complexity Challenge}

\author{Bernhard C. Geiger
\thanks{Bernhard C. Geiger (geiger@ieee.org) was with the Signal Processing and Speech Communication Laboratory, Graz University of Technology, Graz, Austria and is now with the Know-Center GmbH, Graz, Austria.}
}

\newcommand{\dom}[1]{\mathcal{#1}}
\newcommand{\pool}{\dom{P}}
\newcommand{\stake}{r}
\newcommand{\pt}{\tilde{p}}
\newcommand{\kld}[2]{\mathbb{D}_{KL}(#1\Vert #2)}
\newcommand{\jsd}[3]{\mathbb{D}_{JS,#3}(#1,#2)}
\newcommand{\supp}[1]{\mathrm{supp}(#1)}
\newcommand{\Prob}[1]{\mathbb{P}(#1)}
\newcommand{\expec}[1]{\mathbb{E}(#1)}
\newcommand{\avg}[1]{\mathbb{A}\mathrm{vg}(#1)}
\newcommand{\indicator}[1]{\mathbb{I}(#1)}
\newcommand{\todo}[1]{\textcolor{red}{#1}}

\begin{document}
\maketitle

% \begin{abstract}
% This short note summarizes the authors findings during the Spring 2018 Complexity Challenge hosted by the Santa Fe Institute.
% \end{abstract}

% \section{Notation and Assumptions}
There are $N=50$ agents playing a betting game. Agents can place their bets in one of three different pools: A ``stable'' pool $S$, a ``high'' pool $H$, and a ``low'' pool $L$. Let $X_{i,t}$ denote the pool on which agent $i$ bets at time $t$, i.e., $X_{i,t}\in\{S,H,L\}$. Further, let $X_t:=(X_{1,t},\dots,X_{N,t})$ denote the bets of all agents at time $t$. Let $N_{S,t}$, $N_{H,t}$, and $N_{L,t}$ denote the number of agents betting on the stable, high, and low pool at time $t$, respectively.

Every agent in the stable pool receives $\stake_S=1$ at the end of each turn (with probability $p_S=1$); the high and low pools pay $\stake_H=80$ and $\stake_L=40$ with probabilities $p_H=0.25$ and $p_L=0.5$, respectively, and distribute these payments evenly among all agents that bet on it. Thus, the payment $R_{i,t}$ that agent $i$ receives at time $t$ is a random variable characterized by
\begin{align*}
 &\Prob{R_{i,t}=\stake_S|X_{i,t}=S} = p_S=1 &\\
 &\Prob{R_{i,t}=\stake_H/N_{H,t}|X_{i,t}=H} = p_H & \quad \Prob{R_{i,t}=0|X_{i,t}=H} = 1-p_H\\
&\Prob{R_{i,t}=\stake_L/N_{L,t}|X_{i,t}=L} = p_L & \quad \Prob{R_{i,t}=0|X_{i,t}=L} = 1-p_L.
\end{align*}
Let $\bar{R}_t:=\sum_{i=1}^N R_{i,t}$ denote the payment aggregated over all agents.

Every agent may choose a pool at time $t=1$. If an agent wants to switch pools at a later time, it has to pay $\tau\ge 0$. Let $B_{i,t}$ denote the balance of agent $i$ at time $t$, i.e.,
\begin{align*}
 B_{i,t}:=R_{i,1} + \sum_{\ell=2}^t R_{i,\ell} - \tau\cdot \indicator{X_{i,\ell}\neq X_{i,\ell-1}}
\end{align*}
where $\indicator{\cdot}$ is the indicator function. Let $\bar{B}_t:=\sum_{i=1}^N B_{i,t}$ denote the balance aggregated over all agents, and let $\Delta B_{i,t}:=B_{i,t}-B_{i,t-1}$ denote the balance gain of agent $i$ at time $t$ (similarly, $\Delta\bar{B}_t:=\bar{B}_t-\bar{B}_{t-1}$ is the aggregate balance gain). 

Agents may follow a strategy to choose $X_{i,t+1}$ based on their own history, based on the previous numbers of agents in each pool $\{N_{S,1},\dots,N_{S,t}, N_{H,1},\dots\}$, and based on whether or not the high and low pools payed at previous time steps. The game runs for $T=100$ time steps. In order to get consistent results, we averaged the behavior over 500 independent simulation runs and report mean results ($\avg{\cdot}$). Unless otherwise noted, we set $\tau=0.5$.

\section{Theoretical Analysis}
The expected aggregate payment at time $t$ is
\begin{equation}\label{eq:expFixed}
 \expec{\bar{R}_t} = \stake_H p_H \indicator{N_{H,t}\ge 1} + \stake_L p_L \indicator{N_{L,t}\ge 1} + \stake_S N_{S,t}
\end{equation}
which is maximized to $\expec{\bar{R}_t}=88$ if $N_{H,t}=N_{L,t}=1$.

Among other things, we will investigate random strategies in which every agent chooses the next pool randomly according to a given distribution, independently of the current pool and time $t$. Specifically, agent $i$ chooses pool $X\in\{S,H,L\}$ with probability $\mu_X$, where $\mu_X$ does not change over time. Then, the numbers $N_{S,t}$, $N_{H,t}$, and $N_{L,t}$ become random variables and the expected aggregate payment becomes
\begin{equation}\label{eq:expRandom}
 \expec{\bar{R}_t} = \stake_H p_H (1-(1-\mu_H)^N) + \stake_L p_L (1-(1-\mu_L)^N) +\stake_S N(1-\mu_H-\mu_L).
\end{equation}
It can be shown that the expected aggregate payment\footnote{An alternative is to maximize the expected \emph{aggregate balance gain} $\expec{\Delta\bar{B}_t}$ to take the cost of switching into account. We decided against this slightly more complicated optimization problem since it will not significantly change the qualitative statements in this report.} is maximized to $\expec{\bar{R}_t}=82.188$ by setting
\begin{align}\label{eq:bestmu}
 &\mu_H = 1-\sqrt[N-1]{\frac{\stake_S}{\stake_H p_H}} \quad\quad \mu_L = 1-\sqrt[N-1]{\frac{\stake_S}{\stake_L p_L}} \quad\quad\mu_S = 1-\mu_H-\mu_L.
\end{align}

\section{Experimental Analysis: All Agents Have Identical Strategies}

We next evaluate the behavior of agents experimentally. We are particularly interested in the progression of the aggregate balance $\bar{B}_t$, in the balance of individual agents $\bar{B}_{i,t}$, in the fairness of the strategies as measured by the average Gini coefficient of $\{B_{i,100}\}_{i=1,\dots,N}$, and in the distribution of payments $R_{i,t}$ as a function of the strategy. In all strategies considered below, we assume that every agent randomly chooses the initial pool based on the probabilities in~\eqref{eq:bestmu}.

\textbf{The effect of $\tau$.} Since $\tau$ penalizes switching pools, a larger $\tau$ makes agents switch less often. Specifically, the probabilities maximizing $\expec{\Delta\bar{B}_t}$ will have a stronger mode at the stable pool. Suppose a deterministic rule makes agents switch pools in each time step for $\tau$ sufficiently small. Then, increasing $\tau$ first leads to negative balance gains, but then, once a threshold is passed, switching is prevented altogether. Thus, a large $\tau$ can prevent oscillatory behavior and positively affect balance.

\textbf{The effect of $N$.} The strategies we will investigate are largely unaffected by the number of agents $N$.

\subsection{Random Strategies}\label{sec:random}
In our first strategy, \textbf{agents randomly choose pools at each time step}, independently from other agents' or previous choices. Fixing the probabilities as indicated in~\eqref{eq:bestmu} leads to the behavior depicted in Fig.~\ref{fig:RandomIID}. Indeed, by using~\eqref{eq:expRandom} in combination with $\tau=0.5$ and the probabilities in~\eqref{eq:bestmu} shows that the aggregate balance gain satisfies $\avg{\Delta\bar{B}_t}\approx 76.785$, which falls short of $\expec{\bar{R}_t}=82.188$ because of the cost for switching pools. However, by paying this price, the Gini coefficient evaluates to only 0.144, leading to a relatively fair distribution of wealth among the agents.

Our next strategy makes \textbf{agents switch pools until there is exactly one agent each in the low and high pool}, respectively. Specifically, agents choose the next pool according to the following probabilities
\begin{subequations}\label{eq:markov}
\begin{align*}
 &\Prob{X_{i,t}=S|X_{i,t-1}=X} = 1-\frac{1}{N_{X,t-1}}
 &\Prob{X_{i,t}=X|X_{i,t-1}=X} = \frac{1}{N_{X,t-1}}\\
 &\Prob{X_{i,t}=X|X_{i,t-1}=S} = \frac{\indicator{N_{X,t-1}=0}}{N_{S,t-1}}
\end{align*}
for $X=H,L$. In other words, if more than one agent is in the high (low) pool, then all agents in the high (low) pool select their probabilities such that, in expectation, all but one agents move to the stable pool. In contrast, if no agent is in the high (low) pool, then all agents in the stable pool select their probabilities such that, in expectation, one agent fills the empty spot. Indeed, such a strategy leads to excellent performance in terms of aggregate balance as $\avg{\Delta\bar{B}_t}\approx 87.373$. One can further show that this strategy is \emph{asymptotically optimal} in the sense that, for $t\to\infty$, $\Delta\bar{B}_t=88$. However, this comes at the cost of great inequality, leading to a Gini coefficient of 0.411. 

This problem can be partly accounted for by letting \textbf{agents ``push'' into the high and low pools randomly}. Specifically, by setting
\begin{equation*}
 \Prob{X_{i,t}=X|X_{i,t-1}=S} = \max\left\{ \frac{\indicator{N_{X,t-1}=0}}{N_{S,t-1}}, \frac{1}{\kappa N_{S,t-1}} \right\}
\end{equation*}
\end{subequations}
for some $\kappa\in\mathbb{N}$ leads to one agent pushing in the high and low pools, respectively, every $\kappa$ time steps (in expectation). Whenever an agent pushes in an occupied pool, it has a probability of pushing the resident agent out. As our results in Fig.~\ref{fig:maxAgg} show, varying $\kappa$ thus trades between fairness and aggregate balance. Moreover, we conjecture that this strategy is \emph{asymptotically fair} in the sense that balance gain of each agent, averaged over all time steps, converges to the same value (see Fig.~\ref{fig:maxAgg_Long}). The reason is that every agent gets the same chance to push into the high and low pools, respectively, and that, waiting sufficiently long, all agents will do so equally often.

Finally, were there a \textbf{meta-agent coordinating the efforts of all agents and distributing the payments equally}, then the optimal solution would clearly be to let one agent each bet on the low and high pool respectively. Such a scenario, although theoretically little interesting, leads to an aggregate balance of $\Delta\bar{B}_t=88$ and a Gini coefficient of zero.


\subsection{Deterministic Strategies}\label{sec:deterministic}

We next consider the case in which \textbf{every agent stays in the pool that it randomly chose} at $t=1$,  i.e., $X_{i,t+1}=X_{i,t}$ (see Fig.~\ref{fig:Random}). Obviously, the aggregate balance gain this strategy achieves equals the maximum expected aggregate payment, i.e., $\avg{\Delta\bar{B}_t}\approx\expec{\bar{R}_t}=82.188$. However, since most of the agents are stuck in the stable pool, the wealth is distributed unequally leading to a Gini coefficient of 0.342.

In our next strategy all \textbf{agents are greedy}. In other words, an agent decides to bet on the pool that maximizes her expected balance gain in the next step, assuming that no other agent switches pools. Specifically, the expected balance gains for the high, low, and stable pools in the next step of agent $i$ are
\begin{equation}\label{eq:expectedgain}
 \frac{p_Hr_H}{N_{H,t-1}+\indicator{X_{i,t}\neq H}} - \tau \cdot \indicator{X_{i,t}\neq H} \quad\quad \frac{p_Lr_L}{N_{L,t-1}+\indicator{X_{i,t}\neq L}} - \tau \cdot \indicator{X_{i,t}\neq L} \quad\quad r_S- \tau \cdot \indicator{X_{i,t}\neq S}.
\end{equation}
Apparently, such a behavior quickly synchronizes agents: At $t=1$ most agents are in the stable pool, hence the expected payments in the high and low pools outweigh the price of switching pools. Thus, all agents move to either the high or low pools, respectively. Then, if at time $t$ all agents are in the high pool, the expected payment for the next step is small (since $N_{H,t}$ is large). They all thus decide to move to the low pool, since there the expected payment is large (assuming that only one agent moves). This empties the high pool and makes $N_{L,t+1}$ large, which attracts agents again to the high pool and thus repeats the cycle.\footnote{There is a rare but beneficial exception. If equally many agents choose the high and low pools at $t=1$, then these pools are equally attractive for all agents in the stable pool. This leads to an approximately equal distribution of agents over the low and high pools, which makes changing pools unattractive and, thus, leads to a positive development of $\bar{B}_t$. This also explains why the average balance in Fig.~\ref{fig:greedy:Bal} is positive, while the median is negative.} Of course, such a strategy leads to a fair distribution of wealth (with a Gini coefficient of only 0.049), but suffers from the aggregate balance increasing only as $\avg{\Delta\bar{B}_t}\approx 3.05$. Moreover, it can be seen in Fig.~\ref{fig:greedy} that in many cases the balance becomes negative, i.e., the agents lose more than they gain by playing greedily. Indeed, it can be shown that in this synchronized setting we have $\expec{\Delta B_{i,t}}=-0.1$.

The third strategy we investigate assumes that the agents know the average payment per agent, averaged over all previous time steps. The agents are conservative in their betting, i.e., they prefer to bet on the stable pool to get a guaranteed payment. However, if their own balance is below the global average, then they bet \emph{once} on the pool with the largest expected balance gain in the next step and switch back to the stable pool afterwards. In other words, \textbf{agents want fairness}. As expected, their decision to limit excursions to one time step leads to a more fair distribution of wealth (Gini coefficient 0.092), as the low and high pools are cleared after each step. The aggregate balance gain is $\avg{\Delta\bar{B}_t}\approx 44.91$, which outperforms the greedy strategy (see Fig.~\ref{fig:FairnessSafe:bal}). 

More interesting is the assignment of agents to pools shown in Fig.~\ref{fig:FairnessSafe:pool}. This assignment shows both regularity (agents are all in the stable pool in every other time step) and randomness (agents are not synchronized). The fact that agents are not synchronized is a consequence of three facts: 1) $p_H\stake_H = p_L\stake_L$, 2) in every other time step, all agents are in the stable pool, and 3) if two or more pools are equally attractive, agents randomly choose between them. In comparison with the greedy strategy, we conclude that this lack of synchrony leads to improved performance (see also Section~\ref{sec:mixedexperiments}).

Let us investigate the displayed simulation run in detail. At $t=1$, neither the low nor the high pool emitted payments. At $t=2$, at which all agents are in the stable pool, those that bet on the high and low pools in the previous time step observe that their balances are below average. They thus decide to excurse to the high and low pools, which they choose with equal probability. The low pool payed off, which boosted the balance of agents 1 and 31. At $t= 4$, all agents other than 1 and 31 are below average, thus excurse to the high and low pools. Again, the low pool payed off, but not sufficiently to move all its many occupants above average. They thus tried a second time, and those that moved from the low pool to the high pool received payments at time $t=7$ and stayed above average for the remainder of all runs. Also, in Fig.~\ref{fig:FairnessSafe} one can see that several agents bet on the stable pool for a long time, but then have several consecutive excursions to high and low pools. This suggests that the balance gain, averaged over all agents and previous time steps, is greater than $\stake_S=1$.

\subsection{Diversity of Strategies}\label{sec:mixedexperiments}
We finally investigated scenarios in which two subsets of agents follow different strategies. Specifically, suppose that \textbf{the first $N_G$ agents are greedy, while the remaining agents choose their pools randomly in each time step} with probabilities as in~\eqref{eq:bestmu}. Fig.~\ref{fig:greedyrandom:pool} shows that, again, the greedy agents follow a synchronized, regular pattern. More interestingly, Fig.~\ref{fig:greedyrandom:bal} shows how the Gini coefficient and the balance, aggregated over each subset of agents, behave as the number $N_G$ of greedy agents changes. Specifically, a small number of greedy agents can exploit the randomness of the other agents to their advantage while not causing too severe inequality. The more agents become greedy, the more harmful gets this synchronized behavior -- not only for the random agents, but also for the greedy agents and, thus, for all agents as a whole. Indeed, for $N_G=10$, the greedy agents already perform worse than the remaining random agents. If the number of greedy agents increases further, then their balance drops and, as a consequence, the Gini coefficient increases in favor of the random agents. This behavior is visible also in Figs.~\ref{fig:greedyrandom:hist1}-\ref{fig:greedyrandom:hist5} which display the histograms of greedy and random agents for different values of $N_G$.

In the final experiment, we combine \textbf{$N_G$ greedy agents with $N-N_G$ fair agents}, i.e., both strategies are deterministic. Figs.~\ref{fig:greedyfair:pool1}-\ref{fig:greedyfair:pool3} show the assigment of agents to pools. First of all, it can be seen that a single greedy agent does best by staying in the low pool throughout the game. This deterministic behavior introduces synchronization to the fair agents, because the low and the high pool are not equally attractive. The resulting synchronization leads to a severe degradation of the balance aggregated over the fair agents already for $N_G=1$, cf.~Fig.~\ref{fig:greedyfair:bal}. As in the greedy-vs-random scenario, also here a too large number of greedy agents performs worse than the remaining fair agents; the tipping point seems to lie at $N_G=15$, as it can be seen from Fig.~\ref{fig:greedyfair:bal}. In this case, fair agents perform above-average, thus stay in the stable pool (cf.~Fig.~\ref{fig:greedyfair:pool3}), which makes their balance increase faster than the balance of greedy agents.

In both of these scenarios, the situation changes if the greedy agents are \textbf{coordinated by a meta-agent}. For example, the greedy agents will synchronize in a less harmful manner, as the high and low pools will be populated only such that the expected payment exceeds $r_S$. Moreover, the number of times the greedy agents switch pools can be reduced significantly. The group of greedy agents also benefits from \textbf{knowing what pools the random/fair agents will choose} in the next step, as this information admits computing the expected payments of the pools more accurately.

\section{Conclusion and Outlook: Games and POMDPs}

We have seen oscillatory behavior in which agents are synchronized (Fig.~\ref{fig:greedy:pool}), converging behavior (Fig.~\ref{fig:maxAgg:pool1}), and meta-stable states (Fig.~\ref{fig:maxAgg:pool2}). We observed balance gains that are close to the optimum if agent movement is random (iid or Markov), but suboptimal if agent movement is motivated by greed or fairness. Scenarios with diverse strategies, i.e., with some greedy agents among others, are characterized by too many greedy agents harming each other, while few greedy agents can exploit the game. Random behavior seems hard to beat, both in terms of aggregate balance and in terms of fairness.

The next obvious steps are to analyze this agent-based model in a more theoretical framework. For fixed strategies, agent behavior can be modeled as a Markov chain $\{X_t,\ t\in\mathbb{N}\}$ evolving on $\{S,H,L\}^N$. Since this state space is large, aggregation techniques can come into play (e.g.,~\cite{Banisch_MarkovAggregation,Meyn_MarkovAggregation,Amjad_GeneralizedMA}) to investigate macroscopic quantites such as the aggregate balance $\bar{B}_t$~\cite{LamarchePerrin_Aggregation}. Indeed, one can show that the Markov chain defined by~\eqref{eq:markov} is \emph{lumpable} (see~\cite[Def.~6.3.1]{Kemeny_FMC}) w.r.t.\ a partition indicating only the numbers $(N_{H,t},N_{L,t},N_{S,t})$; The state space of the latter has a cardinality less than $N^2$, but admits computing the distribution of $\bar{B}_t$ as accurately as the original Markov chain. 

In the process of developing strategies, the natural next step is to equip the (aggregated) Markov chain with a set of reward functions corresponding to, e.g., fairness and aggregate balance gain $\Delta\bar{B}_t$. Agents may then design strategies in order to maximize these rewards, but may not directly access the state $X_t$; one thus obtains a \emph{partially-observable Markov decision process (POMDP)} and the optimal strategies can be found via heuristics (see, e.g.,~\cite[Ch.~4.6]{Gallager_StochasticProcesses} for Markov decision processes).

Finally, the agents play a \emph{game}, and a strategy optimal for one agent may not be optimal if all agents follow that strategy (see the effects of greed in Figs.~\ref{fig:greedyrandom} and~\ref{fig:greedyfair}). This raises the question if there are strategies that are optimal in the sense that an agent would not change her strategy even if she knew the strategies of other agents; in other words, does the game have Nash equilibria, and if so, what do they look like?

\begin{figure*}[p]
 \centering
 \subfigure[]{\includegraphics[width=0.45\textwidth,trim={0 0.25cm 0 0.5cm},clip]{aggBal_RandomIID}}\hfill
 \subfigure[]{\includegraphics[width=0.45\textwidth,trim={0 0.25cm 0 0.5cm},clip]{Bal_RandomIID}}
 \caption{\textbf{Legend (in this and all subsequent figures):} Solid and dashed blue lines indicate averages and minimum/maximum values achieved during the 500 simulation runs; shaded blue areas indicate the range between the lower and upper quartiles; the solid red line depicts the line $t\cdot\expec{\bar{R}_t}=82.188 t$, cf.~\eqref{eq:expRandom}, assuming the probabilities are chosen according to~\eqref{eq:bestmu}; the dashed red line depicts $t\cdot\expec{\bar{R}_t}=88t$, cf.~\eqref{eq:expFixed}, assuming that $N_{H,t}=N_{L,t}=1$. 
Agents choose pools randomly according to~\eqref{eq:bestmu} at every time step. Since this strategy leads to switching pools many times, it is less efficient on the aggregate level but leads to a fair distribution of wealth among agents. As shown in (b), the balance of agent 1 develops around the average in the majority of simulation runs.}
\label{fig:RandomIID}
\end{figure*}


\begin{figure*}[p]
 \centering
 \subfigure[]{\includegraphics[width=0.45\textwidth,trim={0 0.25cm 0 0.5cm},clip]{aggBal_MaxAgg}}\hfill
 \subfigure[]{\includegraphics[width=0.45\textwidth,trim={0 0.25cm 0 0.5cm},clip]{GI_MaxAgg}}\\
 \subfigure[$\kappa=\infty$\label{fig:maxAgg:pool1}]{\includegraphics[width=0.45\textwidth,trim={0 1.75cm 0 1.75cm},clip]{pool_MaxAgg}}\hfill
 \subfigure[$\kappa=5$\label{fig:maxAgg:pool2}]{\includegraphics[width=0.45\textwidth,trim={0 1.75cm 0 1.75cm},clip]{pool_MaxAgg_random}}
 \caption{Agents choose pools in order to maximize the aggregate balance. As shown in (a), this strategy is successful, outperforming random strategies. This strategy is furthermore asymptotically optimal since it terminates in a situation where the low and high pools contain one agent each (see (c)). Unfortunately, this strategy is also highly unfair, leading to large Gini coefficients. In order to increase fairness, every $\kappa$ steps (in expectation) we give one agent in the stable pool the chance to push into the low and high pool. As it can be seen in (b), this not only lowers the Gini coefficient, but also the average balance per agent at the end of the game. The dashed lines indicate the corresponding results for the random strategy (Fig.~\ref{fig:RandomIID}). (c) and (d) show which pools the agents selected during one simulation run for different values of $\kappa$ (stable pool = yellow, low pool = blue, high pool = green). It can be clearly seen in (d) that agents reside in the high and low pools for a limited time span, being pushed out by agents moving to these pools at random intervals.}
\label{fig:maxAgg}
\end{figure*}

\begin{figure*}[p]
 \centering
 \includegraphics[width=0.75\textwidth,trim={0 0.25cm 0 0.5cm},clip]{GI_MaxAgg_Long}
 \caption{Agents choose pools in order to maximize the aggregate balance, but every $\kappa$ steps we give one agent in the stable pool the chance to push into the low and high pool. This procedure is asymptotically fair if $\kappa$ is finite: All agents in the stable pool have the same probability to push into the low and high pools, and thus have the same chance to push the resident agent out of the pools. The larger $\kappa$ is, the longer it takes for each agent to ``get its share'', but we conjecture that the balance gains, averaged over all time steps, converge to the same value for every agent as $T\to\infty$. The proposed strategy thus admits coming arbitrarily close to maximizing balance and arbitrarily close to distributing fairly simply by letting the game run sufficiently long.}
\label{fig:maxAgg_Long}
\end{figure*}

\begin{figure*}[p]
 \centering
 \subfigure[]{\includegraphics[width=0.45\textwidth]{aggBal_Random}}\hfill
 \subfigure[]{\includegraphics[width=0.45\textwidth]{Bal_Random}}
 \caption{Agents choose pools randomly according to~\eqref{eq:bestmu} at $t=1$ and stay in the chosen pools throughout the remainder of the game. This strategy is efficient on the aggregate level, but suffers from large inequalities among agents. Indeed, most agents are stuck in the stable pool and only few agents accumulate large balances. As shown in (b), the balance of agent 1 shows below-average development in the majority of simulation runs.}
\label{fig:Random}
\end{figure*}

\begin{figure*}[p]
 \centering
 \subfigure[\label{fig:greedy:pool}]{\includegraphics[width=0.45\textwidth]{pool_greedy}}\hfill
 \subfigure[\label{fig:greedy:Bal}]{\includegraphics[width=0.45\textwidth]{Bal_greedy}}
 \caption{Agents choose  pools randomly according to~\eqref{eq:bestmu} at $t=1$ and select the most promising pool (see~\eqref{eq:expectedgain}) in the next steps. (a) shows that this strategy leads to a quick synchronization of agent behavior that leads to a fair payment distribution (Gini coefficient 0.049). The outcome is, in general, poor as it can be seen in (b): In over 75\% of the simulation runs, agent 1 finished with a negative balance.}
\label{fig:greedy}
\end{figure*}

\begin{figure*}[p]
 \centering
 \subfigure[\label{fig:FairnessSafe:pool}]{\includegraphics[width=0.45\textwidth]{pool_FairnessSafe}}\hfill
 \subfigure[\label{fig:FairnessSafe:bal}]{\includegraphics[width=0.45\textwidth]{Bal_FairnessSafe}}
 \caption{Agents choose their pools randomly according to~\eqref{eq:bestmu} at $t=1$. If their balance is below the average balance of the collective, then the agents move to the most promising pool in the next step and move back to the stable pool afterwards. (a) shows that, thanks to all agents moving back to the stable pool and thanks to the fact that both the high and the low pools have identical expected payments, agents are not synchronized. This lack of synchrony leads to acceptable performance in terms of aggregate balance and to an acceptable level of fairness (Gini coefficient 0.092). (b) shows the balance of agent 1. In the best case, the agent started in the high pool with a second agent, received the payment, and thus stayed above average (and in the stable pool) for the remainder of the runs.}
\label{fig:FairnessSafe}
\end{figure*}






\begin{figure*}[p]
 \centering
 \subfigure[$N_G=5$\label{fig:greedyrandom:pool}]{\includegraphics[width=0.45\textwidth]{pool_GRandom}}\hfill
 \subfigure[\label{fig:greedyrandom:bal}]{\includegraphics[width=0.45\textwidth]{GI_GRandom}}\\
 \subfigure[$N_G=1$\label{fig:greedyrandom:hist1}]{\includegraphics[width=0.18\textwidth]{Hist_GRandom1}}\hfill
 \subfigure[$N_G=5$\label{fig:greedyrandom:hist2}]{\includegraphics[width=0.18\textwidth]{Hist_GRandom2}}\hfill
 \subfigure[$N_G=10$\label{fig:greedyrandom:hist3}]{\includegraphics[width=0.18\textwidth]{Hist_GRandom3}}\hfill
 \subfigure[$N_G=20$\label{fig:greedyrandom:hist4}]{\includegraphics[width=0.18\textwidth]{Hist_GRandom4}}\hfill 
\subfigure[$N_G=30$\label{fig:greedyrandom:hist5}]{\includegraphics[width=0.18\textwidth]{Hist_GRandom5}}
 \caption{The first $N_G$ agents are deterministically greedy, while the remaining agents select pools randomly according to~\eqref{eq:bestmu}. (a) shows that the greedy agents synchronize as in Fig.~\ref{fig:greedy:pool}. (b) reveals that greed harms balance when averaged over all agents (solid line with markers). On closer inspection, one can see that few greedy agents can successfully take advantage of the randomness of the remaining agents, in the sense that their average balance (dashed line with markers) exceeds the global average balance and even the average  balance achievable if all agents were acting randomly (horizontal dashed line). If $N_G=10$ agents are greedy, then their average balance falls below the corresponding value for the random agents (dotted line with markers). The reason is that the payments of the low and high pools are now distributed over too many agents to pay off, and this combines unfavorably with the high cost of switching pools too often. It can also be seen that a few greedy agents affect the Gini coefficient less than a large number of them; in the latter case, however, the greedy agents perform worse than the random ones. (c)-(g) display the histograms of the balance gain, averaged over all simulation runs, for the greedy (red) and random agents (blue).}
\label{fig:greedyrandom}
\end{figure*}

\begin{figure*}[p]
 \centering
 \subfigure[$N_G=1$\label{fig:greedyfair:pool1}]{\includegraphics[width=0.45\textwidth]{pool_GFair3}}\hfill
 \subfigure[$N_G=5$\label{fig:greedyfair:pool2}]{\includegraphics[width=0.45\textwidth]{pool_GFair2}}\\
 \subfigure[$N_G=30$\label{fig:greedyfair:pool3}]{\includegraphics[width=0.45\textwidth]{pool_GFair1}}\hfill
 \subfigure[\label{fig:greedyfair:bal}]{\includegraphics[width=0.45\textwidth]{GI_GFair}}\\
 \subfigure[$N_G=1$\label{fig:greedyfair:hist1}]{\includegraphics[width=0.18\textwidth]{Hist_GFair1}}\hfill
 \subfigure[$N_G=5$\label{fig:greedyfair:hist2}]{\includegraphics[width=0.18\textwidth]{Hist_GFair2}}\hfill
 \subfigure[$N_G=10$\label{fig:greedyfair:hist3}]{\includegraphics[width=0.18\textwidth]{Hist_GFair3}}\hfill
 \subfigure[$N_G=20$\label{fig:greedyfair:hist4}]{\includegraphics[width=0.18\textwidth]{Hist_GFair4}}\hfill 
\subfigure[$N_G=30$\label{fig:greedyfair:hist5}]{\includegraphics[width=0.18\textwidth]{Hist_GFair5}}
 \caption{The first $N_G$ agents are deterministically greedy, while the remaining excurse to the low and high pools for one time step, respectively, if their balance is below average. (a) shows that a single greedy agent can exploit the system by always betting on the low pool (or the high pool). This alone prevents other agents from entering this pool since the expected payment is larger in the other pool. If more than one agent is greedy, as in (b) and (c), these agents synchronize and switch between pools constantly, as the other pool always seems to be more attractive. If too many agents are greedy, then the fair agents bet on the stable pool as the expected payment is maximized there (see (c)). (d) shows that greed harms balance when averaged over all agents (solid line with markers). On closer inspection, one can see that few greey agents can successfully take advantage of the randomness of the remaining agents, in the sense that their average balance (dashed line with markers) exceeds the global average balance. (The horizontal dashed line shows the average balance achievable if all agents were acting randomly.) If $N_G=20$ agents are greedy, then their average balance falls below the corresponding value for the fair agents (dotted line with markers). The reason is that the payments of the low and high pools are now distributed over too many agents to pay off, and this combines unfavorably with the high cost of switching pools often. (e)-(i) display the histograms of the balance gain, averaged over all simulation runs, for the greedy (red) and fair agents (blue).}
\label{fig:greedyfair}
\end{figure*}



\bibliographystyle{IEEEtran}
\bibliography{IEEEabrv,references}


\end{document}
