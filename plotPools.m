load('MaxAggPO_5.mat')

for ind=1:50
    runningpool=pool(:,1:50);
    runningpool(:,ind+1:end)=[];
    imagesc(runningpool)
    axis image
    axis tight
    axis([0 50 1 50])
    xlabel('Time $t$','Interpreter','latex')
    ylabel('Agent $i$','Interpreter','latex')
    pause
    print(sprintf('pool_%d',ind),'-depsc')
end

load('MaxAggPO_999.mat')
runningpool=pool(:,1:50);
imagesc(runningpool)
axis image
axis tight
axis([0 50 1 50])
xlabel('Time $t$','Interpreter','latex')
ylabel('Agent $i$','Interpreter','latex')
print('pool','-depsc')