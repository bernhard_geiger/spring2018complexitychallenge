Greedy_numbers=[0 1 2 3 5 10 15 20 25 30 35 40 45 50];
for index = 1:length(Greedy_numbers)
    Greedy_number=Greedy_numbers(index);
    load(sprintf('%dGreedy_Fair.mat',Greedy_number));
    cumPO=cumsum(PO,3);
    finalcumPO=squeeze(cumPO(:,:,end));
    increase(index)=mean(mean(finalcumPO));
    increase_greedy(index)=mean(mean(finalcumPO(:,1:Greedy_number)));
    increase_random(index)=mean(mean(finalcumPO(:,Greedy_number+1:end)));
    if any(any(finalcumPO<0))
        finalcumPO=finalcumPO-min(min(finalcumPO)); % add the lowest value to compute Gini coefficients
    end
    N_runs=size(finalcumPO,1);
    gi=zeros(N_runs,1);
    for ind=1:N_runs
        gi(ind)=gini(ones(1,N),finalcumPO(ind,:));
    end
    GiniCoeff(index)=mean(gi);
    
    if Greedy_number >0 && Greedy_number <50 
        figure
        meanPO=squeeze(mean(PO,3));
        meanGreedy=mean(meanPO(:,1:Greedy_number),2);
        meanUnGreedy=mean(meanPO(:,Greedy_number+1:end),2);
        histf(meanGreedy,[min(meanGreedy):0.1:max(meanGreedy)],'facecolor','r','facealpha',.5,'edgecolor','none')
        hold on
        histf(meanUnGreedy,[min(meanUnGreedy):0.1:max(meanUnGreedy)],'facecolor','b','facealpha',.5,'edgecolor','none')
        xlabel('Average Net Balance Gain','Interpreter','latex')
        ylabel('Histogram','Interpreter','latex')
        axis tight
%         legend('Greedy Agents','Random Agents')
    end
end

figure
yyaxis left
plot(Greedy_numbers,GiniCoeff,'-x')
hold on
plot(Greedy_numbers,ones(1,length(Greedy_numbers))*0.144,'--')
xlabel('Number of Greedy Agents','Interpreter','latex')
ylabel('Gini Coefficient','Interpreter','latex')

yyaxis right
h1=plot(Greedy_numbers,increase,'-x');
h2=plot(Greedy_numbers,increase_greedy,'--x');
h3=plot(Greedy_numbers,increase_random,':x');
hold on
plot(Greedy_numbers,ones(1,length(Greedy_numbers))*2*76.785,'--')
axis([0 50 0 300])
legend([h1,h2,h3],'Global', 'Greedy','Fair')
ylabel('Average Balance $\bar{B}_{100}$','Interpreter','latex')