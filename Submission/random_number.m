function n=random_number(p)

x=rand(1);
p=cumsum(p);
n=find(x<p,1,'first');