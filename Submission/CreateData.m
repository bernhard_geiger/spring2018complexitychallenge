%% Complexity Challenge

clear variables;
close all;
clc;

%% Setup 
% Pools: 1: Low, 2: High, 3: Stable

N=50;       % no. of agents
T=100;      % no. of time-steps
tau=0.5;    % cost for swapping
N_runs=500; % number of simulation runs for averaging

stablePO=1;
highPO=80; highProb=0.25;
lowPO=40; lowProb=0.5;
PayOff_vec=[lowPO*lowProb highPO*highProb stablePO];

% maximum aggregate PO per time-step
aggPO_max=highPO*highProb+lowPO*lowProb+(N-2)*stablePO;

% maximum individual PO per time-step
indivPO_max=max([highPO*highProb,lowPO*lowProb,stablePO]);

% optimal probabilities to maximize expected PO
pl=1-(1/(lowPO*lowProb))^(1/(N-1));
ph=1-(1/(highPO*highProb))^(1/(N-1));
aggPO_exp=(1-(1-pl)^N)*lowPO*lowProb+(1-(1-ph)^N)*highPO*highProb+N*(1-pl-ph);

%% Agents choose pool randomly once and stick to it. 
pool=zeros(N,T);
N_vector=zeros(T,3);
PO=zeros(N_runs,N,T);
for cnt=1:N_runs
    for timestep=1:T
        for agent=1:N   % Choose pool
            if timestep==1
                pool(agent,timestep)=random_number([pl, ph, 1-pl-ph]);
            else
                pool(agent,timestep)=pool(agent,timestep-1);
            end
        end            
        
        % Compute common knowledge
        N_vector(timestep,1)=sum(pool(:,timestep)==1);
        N_vector(timestep,2)=sum(pool(:,timestep)==2);
        N_vector(timestep,3)=sum(pool(:,timestep)==3);
        
        PayLow=random_number([1-lowProb lowProb])-1;
        PayHigh=random_number([1-highProb highProb])-1;
        for agent=1:N   % Compute payoffs
            if pool(agent,timestep)==3
                PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+1;
            elseif pool(agent,timestep)==1
                PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+PayLow*lowPO/max(1,N_vector(timestep,1));
            elseif pool(agent,timestep)==2
                PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+PayHigh*highPO/max(1,N_vector(timestep,2));
            end
        end           
    end
end

% plot_PO(PO,aggPO_exp,aggPO_max,aggPO_exp/N,indivPO_max)
save('random_stay.mat')

%% Agents choose pool randomly every time step, pay tau every time they switch.
pool=zeros(N,T);
N_vector=zeros(T,3);
PO=zeros(N_runs,N,T);
for cnt=1:N_runs
    for timestep=1:T
        for agent=1:N   % Choose pool
        	pool(agent,timestep)=random_number([pl, ph, 1-pl-ph]);
            if timestep>1
                if pool(agent,timestep)~=pool(agent,timestep-1)
                    PO(cnt,agent,timestep)=PO(cnt,agent,timestep)-tau;
                end
            end
        end            
        
        % Compute common knowledge
        N_vector(timestep,1)=sum(pool(:,timestep)==1);
        N_vector(timestep,2)=sum(pool(:,timestep)==2);
        N_vector(timestep,3)=sum(pool(:,timestep)==3);
        
        PayLow=random_number([1-lowProb lowProb])-1;
        PayHigh=random_number([1-highProb highProb])-1;
        for agent=1:N   % Compute payoffs
            if pool(agent,timestep)==3
                PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+1;
            elseif pool(agent,timestep)==1
                PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+PayLow*lowPO/max(1,N_vector(timestep,1));
            elseif pool(agent,timestep)==2
                PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+PayHigh*highPO/max(1,N_vector(timestep,2));
            end
        end       
    end
end

% plot_PO(PO,aggPO_exp,aggPO_max,aggPO_exp/N,indivPO_max)
save('random_iid.mat')

%% Agents play safe but want fairness.
% If an agent is in the stable pool and below average, it moves to the pool
% with the greatest expected PO for exactly one timestep. If an agent is
% above average, it stays in
% the stable pool. They assume that no other agent moves.
pool=zeros(N,T);
N_vector=zeros(T,3);
PO=zeros(N_runs,N,T);
average=0;
for cnt=1:N_runs
    for timestep=1:T
        if timestep==1
             for agent=1:N   % Choose pool
                 pool(agent,timestep)=random_number([pl, ph, 1-pl-ph]);
             end
        else
            for agent=1:N   % Choose pool
                if mean(squeeze(PO(cnt,agent,1:timestep-1))) < average && pool(agent,timestep-1)==3
                    pool(agent,timestep)=chooseBestPool(PayOff_vec,N_vector(timestep-1,:),pool(agent,timestep-1),2*tau);
                else
                    pool(agent,timestep)=3;
                end
                if pool(agent,timestep)~=pool(agent,timestep-1)
                    PO(cnt,agent,timestep)=PO(cnt,agent,timestep)-tau;
                end
            end      
        end
        
        % Compute common knowledge
        N_vector(timestep,1)=sum(pool(:,timestep)==1);
        N_vector(timestep,2)=sum(pool(:,timestep)==2);
        N_vector(timestep,3)=sum(pool(:,timestep)==3);
        
        PayLow=random_number([1-lowProb lowProb])-1;
        PayHigh=random_number([1-highProb highProb])-1;
        for agent=1:N   % Compute payoffs
            if pool(agent,timestep)==3
                PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+1;
            elseif pool(agent,timestep)==1
                PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+PayLow*lowPO/max(1,N_vector(timestep,1));
            elseif pool(agent,timestep)==2
                PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+PayHigh*highPO/max(1,N_vector(timestep,2));
            end
        end     
        
        average=mean(mean(squeeze(PO(cnt,:,1:timestep-1)),2));
    end
end

% plot(cumsum(squeeze(PO(1,1,:))))
plot_PO(PO,aggPO_exp,aggPO_max,aggPO_exp/N,indivPO_max)
save('FairnessSafe2.mat')

%% Agents want safe-fairness, but some are greedy
% Agents proceed as in the last scenario, but some agents are greedy. They
% move to the pool with the largest expected outcome in the next step.
HowManyGreedy=[0 1 2 3 5 10 15 20 25 30 35 40 45 50];

for N_G=HowManyGreedy
    pool=zeros(N,T);
    N_vector=zeros(T,3);
    PO=zeros(N_runs,N,T);
    for cnt=1:N_runs
        for timestep=1:T
            if timestep==1
                 for agent=1:N   % Choose pool
                     pool(agent,timestep)=random_number([pl, ph, 1-pl-ph]);
                 end
            else
                for agent=N_G+1:N   % Choose pool
                    if mean(squeeze(PO(cnt,agent,1:timestep-1))) < average && pool(agent,timestep-1)==3
                        pool(agent,timestep)=chooseBestPool(PayOff_vec,N_vector(timestep-1,:),pool(agent,timestep-1),tau);
                    else
                        pool(agent,timestep)=3;
                    end
                    if pool(agent,timestep)~=pool(agent,timestep-1)
                        PO(cnt,agent,timestep)=PO(cnt,agent,timestep)-tau;
                    end
                end      
                for agent=1:N_G
                    pool(agent,timestep)=chooseBestPool(PayOff_vec,N_vector(timestep-1,:),pool(agent,timestep-1),tau);
                    if pool(agent,timestep)~=pool(agent,timestep-1)
                        PO(cnt,agent,timestep)=PO(cnt,agent,timestep)-tau;
                    end
                end
            end

            % Compute common knowledge
            N_vector(timestep,1)=sum(pool(:,timestep)==1);
            N_vector(timestep,2)=sum(pool(:,timestep)==2);
            N_vector(timestep,3)=sum(pool(:,timestep)==3);

            PayLow=random_number([1-lowProb lowProb])-1;
            PayHigh=random_number([1-highProb highProb])-1;
            for agent=1:N   % Compute payoffs
                if pool(agent,timestep)==3
                    PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+1;
                elseif pool(agent,timestep)==1
                    PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+PayLow*lowPO/max(1,N_vector(timestep,1));
                elseif pool(agent,timestep)==2
                    PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+PayHigh*highPO/max(1,N_vector(timestep,2));
                end
            end       

            average=mean(mean(squeeze(PO(cnt,:,1:timestep-1)),2));
        end
    end

%     plot_PO(PO,aggPO_exp,aggPO_max,aggPO_exp/N,indivPO_max)
%     figure
%     meanPO=squeeze(mean(PO,3));
%     subplot(211)
%     imagesc(meanPO)
%     load MyColormap.mat
%     colormap(mymap)
%     subplot(212)
%     meanGreedy=mean(meanPO(:,1:N_G),2);
%     meanUnGreedy=mean(meanPO(:,N_G+1:end),2);
%     histf(meanGreedy,[min(meanGreedy):0.1:max(meanGreedy)],'facecolor','r','facealpha',.5,'edgecolor','none')
%     hold on
%     histf(meanUnGreedy,[min(meanUnGreedy):0.1:max(meanUnGreedy)],'facecolor','b','facealpha',.5,'edgecolor','none')
%     xlabel('Average Payoff/Timestep')
%     ylabel('Histogram')
%     axis tight
%     legend('Greedy Agents','Random Agents')
    save(sprintf('%dGreedy_Fair.mat',N_G))
end

%% Agents are random, some are greedy
% Agents move randomly, but some agents are greedy. They
% move to the pool with the largest expected outcome in the next step.
for N_G=HowManyGreedy
    pool=zeros(N,T);
    N_vector=zeros(T,3);
    PO=zeros(N_runs,N,T);
    for cnt=1:N_runs
        for timestep=1:T
            if timestep==1
                 for agent=1:N   % Choose pool
                     pool(agent,timestep)=random_number([pl, ph, 1-pl-ph]);
                 end
            else
                for agent=N_G+1:N 
                    pool(agent,timestep)=random_number([pl, ph, 1-pl-ph]);
                    if pool(agent,timestep)~=pool(agent,timestep-1)
                        PO(cnt,agent,timestep)=PO(cnt,agent,timestep)-tau;
                    end
                end      
                for agent=1:N_G
                    pool(agent,timestep)=chooseBestPool(PayOff_vec,N_vector(timestep-1,:),pool(agent,timestep-1),tau);
                    if pool(agent,timestep)~=pool(agent,timestep-1)
                        PO(cnt,agent,timestep)=PO(cnt,agent,timestep)-tau;
                    end
                end
            end

            % Compute common knowledge
            N_vector(timestep,1)=sum(pool(:,timestep)==1);
            N_vector(timestep,2)=sum(pool(:,timestep)==2);
            N_vector(timestep,3)=sum(pool(:,timestep)==3);

            PayLow=random_number([1-lowProb lowProb])-1;
            PayHigh=random_number([1-highProb highProb])-1;
            for agent=1:N   % Compute payoffs
                if pool(agent,timestep)==3
                    PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+1;
                elseif pool(agent,timestep)==1
                    PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+PayLow*lowPO/max(1,N_vector(timestep,1));
                elseif pool(agent,timestep)==2
                    PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+PayHigh*highPO/max(1,N_vector(timestep,2));
                end
            end       
        end
    end

%     plot_PO(PO,aggPO_exp,aggPO_max,aggPO_exp/N,indivPO_max)
%     figure
%     meanPO=squeeze(mean(PO,3));
%     subplot(211)
%     imagesc(meanPO)
%     load MyColormap.mat
%     colormap(mymap)
%     subplot(212)
%     meanGreedy=mean(meanPO(:,1:N_G),2);
%     meanUnGreedy=mean(meanPO(:,N_G+1:end),2);
%     histf(meanGreedy,[min(meanGreedy):0.1:max(meanGreedy)],'facecolor','r','facealpha',.5,'edgecolor','none')
%     hold on
%     histf(meanUnGreedy,[min(meanUnGreedy):0.1:max(meanUnGreedy)],'facecolor','b','facealpha',.5,'edgecolor','none')
%     xlabel('Average Payoff/Timestep')
%     ylabel('Histogram')
%     axis tight
%     legend('Greedy Agents','Random Agents')
    save(sprintf('%dGreedy_Random.mat',N_G))
end

%% Agents want to maximize aggregate PO, but want to play fair.
% If more than one agent is in one of the high-paying pools, then all
% agents move with a probability such that one is expected to stay. Agents
% in the stable pool move such that, on average, 1/NUMBER agents move to another
% pool.
move_numbers=[1 2 5 10 20 inf];
for Move_Number = move_numbers
    pool=zeros(N,T);
    N_vector=zeros(T,3);
    PO=zeros(N_runs,N,T);
    average=0;
    for cnt=1:N_runs
        for timestep=1:T
            if timestep==1
                 for agent=1:N   % Choose pool
                     pool(agent,timestep)=random_number([pl, ph, 1-pl-ph]);
                 end
            else
                for agent=1:N   % Choose pool
                    pool(agent,timestep)=pool(agent,timestep-1);
                    switch pool(agent,timestep-1)
                        case 1
                            pool(agent,timestep)=random_number([1/N_vector(timestep-1,1), 0 1-1/N_vector(timestep-1,1)]);
                        case 2
                            pool(agent,timestep)=random_number([0, 1/N_vector(timestep-1,2), 1-1/N_vector(timestep-1,2)]);
                        case 3
                            move_to_1=max(1/N_vector(timestep-1,3)/Move_Number,max(0,1-N_vector(timestep-1,1))/N_vector(timestep-1,3));
                            move_to_2=max(1/N_vector(timestep-1,3)/Move_Number,max(0,1-N_vector(timestep-1,2))/N_vector(timestep-1,3));
                            pool(agent,timestep)=random_number([move_to_1,move_to_2, 1-move_to_1-move_to_2 ]);
                    end          

                    if pool(agent,timestep)~=pool(agent,timestep-1)
                        PO(cnt,agent,timestep)=PO(cnt,agent,timestep)-tau;
                    end
                end      
            end

            % Compute common knowledge
            N_vector(timestep,1)=sum(pool(:,timestep)==1);
            N_vector(timestep,2)=sum(pool(:,timestep)==2);
            N_vector(timestep,3)=sum(pool(:,timestep)==3);

            PayLow=random_number([1-lowProb lowProb])-1;
            PayHigh=random_number([1-highProb highProb])-1;
            for agent=1:N   % Compute payoffs
                if pool(agent,timestep)==3
                    PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+1;
                elseif pool(agent,timestep)==1
                    PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+PayLow*lowPO/max(1,N_vector(timestep,1));
                elseif pool(agent,timestep)==2
                    PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+PayHigh*highPO/max(1,N_vector(timestep,2));
                end
            end     
        end
    end

    % plot_PO(PO,aggPO_exp,aggPO_max,aggPO_exp/N,indivPO_max)
    save(sprintf('MaxAggPO_%d.mat',min(999,Move_Number)))
end

%% Agents want to maximize aggregate PO, but want to play fair.
% If more than one agent is in one of the high-paying pools, then all
% agents move with a probability such that one is expected to stay. Agents
% in the stable pool move such that, on average, 1/NUMBER agents move to another
% pool.
T=1000;
move_numbers=[1 5 20];
for Move_Number = move_numbers
    pool=zeros(N,T);
    N_vector=zeros(T,3);
    PO=zeros(N_runs,N,T);
    average=0;
    for cnt=1:N_runs
        for timestep=1:T
            if timestep==1
                 for agent=1:N   % Choose pool
                     pool(agent,timestep)=random_number([pl, ph, 1-pl-ph]);
                 end
            else
                for agent=1:N   % Choose pool
                    pool(agent,timestep)=pool(agent,timestep-1);
                    switch pool(agent,timestep-1)
                        case 1
                            pool(agent,timestep)=random_number([1/N_vector(timestep-1,1), 0 1-1/N_vector(timestep-1,1)]);
                        case 2
                            pool(agent,timestep)=random_number([0, 1/N_vector(timestep-1,2), 1-1/N_vector(timestep-1,2)]);
                        case 3
                            move_to_1=max(1/N_vector(timestep-1,3)/Move_Number,max(0,1-N_vector(timestep-1,1))/N_vector(timestep-1,3));
                            move_to_2=max(1/N_vector(timestep-1,3)/Move_Number,max(0,1-N_vector(timestep-1,2))/N_vector(timestep-1,3));
                            pool(agent,timestep)=random_number([move_to_1,move_to_2, 1-move_to_1-move_to_2 ]);
                    end          

                    if pool(agent,timestep)~=pool(agent,timestep-1)
                        PO(cnt,agent,timestep)=PO(cnt,agent,timestep)-tau;
                    end
                end      
            end

            % Compute common knowledge
            N_vector(timestep,1)=sum(pool(:,timestep)==1);
            N_vector(timestep,2)=sum(pool(:,timestep)==2);
            N_vector(timestep,3)=sum(pool(:,timestep)==3);

            PayLow=random_number([1-lowProb lowProb])-1;
            PayHigh=random_number([1-highProb highProb])-1;
            for agent=1:N   % Compute payoffs
                if pool(agent,timestep)==3
                    PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+1;
                elseif pool(agent,timestep)==1
                    PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+PayLow*lowPO/max(1,N_vector(timestep,1));
                elseif pool(agent,timestep)==2
                    PO(cnt,agent,timestep)=PO(cnt,agent,timestep)+PayHigh*highPO/max(1,N_vector(timestep,2));
                end
            end     
        end
    end

    % plot_PO(PO,aggPO_exp,aggPO_max,aggPO_exp/N,indivPO_max)
    save(sprintf('Long_MaxAggPO_%d.mat',min(999,Move_Number)))
end