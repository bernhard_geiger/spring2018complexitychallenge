move_numbers=[1 2 5 10 20 inf];
for index = 1:length(move_numbers)
    Move_Number=move_numbers(index);
    load(sprintf('MaxAggPO_%d.mat',min(999,Move_Number)));
    cumPO=cumsum(PO,3);
    finalcumPO=squeeze(cumPO(:,:,end));
    N_runs=size(finalcumPO,1);
    gi=zeros(N_runs,1);
    for ind=1:N_runs
        gi(ind)=gini(ones(1,N),finalcumPO(ind,:));
    end
    GiniCoeff(index)=mean(gi);
    increase(index)=mean(mean(finalcumPO));
end

figure
yyaxis left
plot(1:length(move_numbers),GiniCoeff)
hold on
plot(1:length(move_numbers),ones(1,length(move_numbers))*0.144,'--')
xlabel('$\kappa$','Interpreter','latex')
xticklabels({'1', '2', '5', '10', '20', '\infty'})
ylabel('Gini Coefficient','Interpreter','latex')

yyaxis right
plot(1:length(move_numbers),increase)
hold on
plot(1:length(move_numbers),ones(1,length(move_numbers))*76.785*2,'--')
ylabel('Average Balance $\bar{B}_{100}$','Interpreter','latex')