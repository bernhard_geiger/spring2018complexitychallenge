function plot_PO(PO,aggPO_exp,aggPO_max,indivPO_exp,indivPO_max)

T=size(PO,3);
N_runs=size(PO,1);
N=size(PO,2);
cumPO=cumsum(PO,3);

figure
% subplot(221)
aggPO=squeeze(sum(cumPO,2));
shadedErrorBar(1:T,aggPO,{'quantiles',0.25},{'-b','LineWidth',2},1);
hold on
plot(1:T,mean(aggPO),'b','LineWidth',2)
plot(1:T,min(aggPO),'b--','LineWidth',2)
plot(1:T,max(aggPO),'b--','LineWidth',2)
plot(1:T,[1:T]*aggPO_exp,'r','LineWidth',1)
plot(1:T,[1:T]*aggPO_max,'r--','LineWidth',1)
xlabel('Time $t$','Interpreter','latex')
ylabel('Aggregated Balance $\overline{B}_t$','Interpreter','latex')

figure
% subplot(222)
A1PO=squeeze(cumPO(:,1,:));
shadedErrorBar(1:T,A1PO,{'quantiles',0.25},{'-b','LineWidth',2},1);
hold on
plot(1:T,mean(A1PO),'b','LineWidth',2)
plot(1:T,min(A1PO),'b--','LineWidth',2)
plot(1:T,max(A1PO),'b--','LineWidth',2)
plot(1:T,[1:T]*indivPO_exp,'r','LineWidth',1)
xlabel('Time $t$','Interpreter','latex')
ylabel('Balance $B_{1,t}$','Interpreter','latex')
axis([0 T 0 200])


figure
% subplot(2,2,3)
meanPO=squeeze(mean(PO,3));
[maxPO,~]=max(meanPO,[],2);
[minPO,~]=min(meanPO,[],2);
histf(maxPO,[min(maxPO):0.1:max(maxPO)],'facecolor','b','facealpha',.5,'edgecolor','none')
hold on
histf(minPO,[min(minPO):0.1:max(minPO)+0.1],'facecolor','r','facealpha',.5,'edgecolor','none')
xlabel('Average Payoff/Timestep')
ylabel('Histogram')
axis tight
legend('Best Agent','Worst Agent')

figure
% subplot(2,2,4)
finalcumPO=squeeze(cumPO(:,:,end));
N_runs=size(finalcumPO,1);
gi=[];
if any(any(finalcumPO<0))
 finalcumPO=finalcumPO-min(min(finalcumPO)); % add the lowest value to compute Gini coefficients
end
for ind=1:N_runs
    if ~any(finalcumPO(ind,:)<0)
        gi=[gi gini(ones(1,N),finalcumPO(ind,:))];
    end
end
boxplot(gi)
title(sprintf('Mean Gini Coefficient: %0.3f',mean(gi)))
ylabel('Distribution of Gini Coefficients')


