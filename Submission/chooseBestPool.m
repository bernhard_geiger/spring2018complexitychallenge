function pool=chooseBestPool(Payoff,N_pool,currentPool,tau)

total_vec=Payoff;
switch currentPool
    case 1
        total_vec(1)=total_vec(1)/N_pool(1);
        total_vec(2)=total_vec(2)/(N_pool(2)+1)-tau;
        total_vec(3)=total_vec(3)-tau;
    case 2
        total_vec(2)=total_vec(2)/N_pool(2);
        total_vec(1)=total_vec(1)/(N_pool(1)+1)-tau;
        total_vec(3)=total_vec(3)-tau;
    case 3
        total_vec(1)=total_vec(1)/(N_pool(1)+1)-tau;
        total_vec(2)=total_vec(2)/(N_pool(2)+1)-tau;
end

pools=find(total_vec==max(total_vec));
pool=pools(randi(1:length(pools)));